import {Column, Entity, PrimaryGeneratedColumn} from 'typeorm';

@Entity('shopping')
export class shoppingSave {

    @PrimaryGeneratedColumn()
    id: string;

    @Column()
    values: string;

    @Column()
    time: string;

}