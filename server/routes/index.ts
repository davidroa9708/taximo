import * as express from "express";
import shopping from "../controllers/shopping";

export const register = ( app: express.Application ) => {
    const Shopping = new shopping();

    // define a route handler for the request
    app.post( "/insertar/", Shopping.setShop);
};
