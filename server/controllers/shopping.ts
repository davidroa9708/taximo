import { Request, Response } from "express";
import {getManager, Repository,getConnection} from "typeorm";

import {shoppingSave} from "../entity/shoppingSave";
import * as fs from 'fs';
import { validate, ValidationError } from 'class-validator';

export default class shopping {

  
   public static async Calculate(req: Request, res: Response) {
          var input = req.body.data;
          const startTime = Date.now();
          const logging = false;
          var helperMap = [];
          var arr = input.split("\n");
          var malls = [];
          var [N, M, K] = arr[0].split(" ").map(Number);
          var allFishes = (1 << K) - 1;
          for (var i = 1; i <= N; i++) {
              malls.push({
                  fishes: arr[i].split(" ").slice(1).map(Number).reduce((fishes, fish) => {
                      fishes += 1 << (fish - 1);
                      return fishes;
                  }, 0),
                  roads: [],
                  distanceToFinish: 0,
              });
          }
          for (let i = N + 1; i <= N + M; i++) {
              let [idx1, idx2, time] = arr[i].split(" ").map(Number);
              idx1--;
              idx2--;
              malls[idx1].roads.push([idx2, time]);
              malls[idx2].roads.push([idx1, time]);
          }
          malls.forEach((mall, idx) => {
              if (idx === 0 || idx === N - 1)
                  return;
              if (mall.fishes === 0 && mall.roads.length < 4) {
                  //console.log(mall);
                  for (var i = 0; i < mall.roads.length; i++) {
                      var availableMall = malls[mall.roads[i][0]];
                      var roadToHereIndex = availableMall.roads.findIndex((r) => r[0] === idx);
                      var roadToHere = availableMall.roads.splice(roadToHereIndex, 1)[0];
                      for (var j = 0; j < mall.roads.length; j++) {
                          if (i !== j) {
                              var otherRoad = mall.roads[j];
                              var shouldAdd = true;
                              for (var availableMallRoad of availableMall.roads) {
                                  if (availableMallRoad[0] === otherRoad[0]) {
                                      availableMallRoad[1] = Math.min(roadToHere[1] + otherRoad[1], availableMallRoad[1]);
                                      shouldAdd = false;
                                      break;
                                  }
                              }
                              if (shouldAdd) {
                                  availableMall.roads.push([otherRoad[0], roadToHere[1] + otherRoad[1]]);
                              }
                          }
                      }
                  }
              }
          });
          malls.forEach((mall) => mall.roads.sort((a, b) => a[1] - b[1]));
          var minTime = 0;
          malls.slice(-1).forEach((mall, rootIdx) => {
              var visited = [];
              visited[malls.length - 1] = 0;
              const queue = [];
              for (let c of mall.roads) {
                  queue.push(...c);
                  visited[c[0]] = c[1];
              }
              while (queue.length > 0) {
                  const idx = queue.shift();
                  const time = queue.shift();
                  const visitingMall = malls[idx];
                  visitingMall.distanceToFinish = time;
                  for (let c of visitingMall.roads) {
                      logging && console.log("step from", idx, "to", c[0], "time:", c[1] + time, visited[c[0]]);
                      if (visited[c[0]] === undefined || visited[c[0]] > c[1] + time) {
                          logging && console.log("pf");
                          visited[c[0]] = c[1] + time;
                          queue.push(c[0], c[1] + time);
                      }
                  }
              }
          });
          const visited = [];
          var tempFish = 0;
          minTime = malls[0].distanceToFinish;
          for (var i = 0; i < malls.length; i++) {
              var mall = malls[i];
              if (i !== 0 && (mall.fishes | tempFish) > tempFish) {
                  minTime += 2 * mall.distanceToFinish;
              }
              visited[i] = [];
              mall.roads = mall.roads.sort((a, b) => {
                  var distToFinishThroughA = a[1] + malls[a[0]].distanceToFinish;
                  var distToFinishThroughB = b[1] + malls[b[0]].distanceToFinish;
                  return distToFinishThroughA - distToFinishThroughB;
              });
          }
          logging && console.error(JSON.stringify(malls, null, 2));
          var step = 0;
          var mall = malls[0];
          visited[0][mall.fishes] = 0;
          const queue = [];
          for (let c of mall.roads) {
              const newFishes = mall.fishes | malls[c[0]].fishes;
              const visitTime = c[1];
              const visitMallIdx = c[0];
              queue.push(visitMallIdx, visitTime, newFishes);
              visited[visitMallIdx] = [];
              visited[visitMallIdx][newFishes] = visitTime;
          }
          let cat;
          const log = (...args) => {
              const loggerFunction = cat ? console.error : console.log;
              loggerFunction(...[`Cat #${cat}`].concat(args));
          };
          const fishTree = {};
          const fishMap = {};
          var qidx = -3;
          while (qidx + 3 < queue.length) {
              step += 1;
              qidx += 3;
              cat = step % 2;
              var otherCat = 1 - cat;
              const idx = queue[qidx];
              const time = queue[qidx + 1];
              const visitingMall = malls[idx];
              const fish = queue[qidx + 2];
              if (time + visitingMall.distanceToFinish >= minTime) {
                  logging && log("continue", time, visitingMall.distanceToFinish);
                  continue;
              }
              const fishRecord = fishMap[fish];
              if (fishRecord === undefined || fishRecord > time + visitingMall.distanceToFinish) {
                  fishMap[fish] = time + visitingMall.distanceToFinish;
                  for (var f in fishMap) {
                      if ((parseInt(f) | fish) === allFishes) {
                          //console.log("OOOO " + step, time + visitingMall.distanceToFinish, fish.toString(2), fishMap[f], parseInt(f).toString(2));
                          minTime = Math.min(minTime || Infinity, Math.max(fishMap[f], time + visitingMall.distanceToFinish));
                      }
                  }
              }
              for (const c of visitingMall.roads) {
                  const roadMall = malls[c[0]];
                  if (time + roadMall.distanceToFinish >= minTime) {
                      continue;
                  }
                  const futureFish = fish | roadMall.fishes;
                  const lastVisitThereWithSameFishTime = visited[c[0]][futureFish];
                  if (lastVisitThereWithSameFishTime === undefined || lastVisitThereWithSameFishTime > c[1] + time) {
                      logging && log(" should visit " + c[0], " now we have " + fish.toString(2), " but it will be " + futureFish.toString(2));
                      visited[c[0]][futureFish] = c[1] + time;
                      queue.push(c[0], c[1] + time, futureFish);
                  }
              }
          }
          console.log(req.body.data);
          return minTime;
      
   }


   public async setShop(req: Request, res: Response) {
      const data = req.body.data;

      await shopping.cleanData(data, req, res);
      var q = await shopping.Validate(data);
      //const respponse = await shopping.minTime(data);
      let t ;
      if (q.length > 0) {
        t = q[0].time;
      }else{
        await shopping.Calculate(req, res)
                    .then(async (response) => {
                        t = response;
                        await shopping.saveResult(data, response);
                    });
      }
      return res.json(t);
   }
   public static async cleanData(data: string,req: Request, res: Response) {
        data.split(' ').forEach((element) => {
            if (!parseInt(element)) {
                return res.json({"error":"Datos erroneos"});
            }
        });
   }

   public static async Validate(data) {
      return await getManager()
            .query(
                'SELECT time FROM "public"."shopping" WHERE values = $1'
                , [
                  data
                ]
            );
   }

   public static async saveResult(data, time: number) {
        const ShopRepository: Repository<shoppingSave> = getManager()
            .getRepository(shoppingSave);

        const dataToBeSaved: shoppingSave = new shoppingSave();

        dataToBeSaved.values = data;
        dataToBeSaved.time = time.toString();

        await ShopRepository.save(dataToBeSaved);
        
    }
    
}
