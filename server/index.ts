import * as dotenv from 'dotenv';
import * as express from "express";
import * as bodyParser from "body-parser";
import * as cors from "cors";
import * as routes from "./routes";
import 'reflect-metadata';
import {createConnection} from "typeorm";

dotenv.config();

createConnection({
    type: "postgres",
    host: "localhost",
    port: 5432,
    username: "taximo_jorge",
    password: "Admi123*",
    database: "taximo_jorge",
    entities: [
        'server/entity/**/*.ts'
    ],
    synchronize: true,
    logging: false
}).then(connection => {

// initialize configuration
const app = express();

// Cinfugure Port
app.set("port", process.env.SERVER_PORT || 3000);

// Configure Express to parse incoming JSON data
app.use( express.json() );

//router.use(cors(options));
app.use(cors());

// Configure routes
routes.register( app );

// Enable bodyParser with default options
app.use(bodyParser.urlencoded({ extended: true }));
 

// start the Express server
app.listen( app.get("port"), () => {
    // tslint:disable-next-line:no-console
    console.log( `server started at http://localhost:` + app.get("port")  );
} );
    
}).catch(error => console.log(error));
