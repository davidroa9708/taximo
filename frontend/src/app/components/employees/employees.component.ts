import { Component, OnInit } from '@angular/core';

import { EmployeeService } from '../../services/employee.service';
import { NgForm } from '@angular/forms';
import { Employee } from '../../models/employee';

declare var M:any;

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css'],
  providers: [EmployeeService]
})
export class EmployeesComponent implements OnInit {

  public response;
  
  constructor(private employeeService: EmployeeService) { }

  ngOnInit() {
  	
  }

  resetForm(form?: NgForm){
  	if (form) {
      this.employeeService.postShopping(form.value)
        .subscribe((res) => {
            this.response = res;
      });
  	}
  }

}
