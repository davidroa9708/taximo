import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Employee } from '../models/employee';
import { EmployeesComponent } from '../components/employees/employees.component';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

	selectedEmployee: Employee;
	employees: Employee[];
	readonly URL_API='http://192.168.99.100:3000/insertar';

  	constructor(private http: HttpClient) {
  		this.selectedEmployee = new Employee();
  	}
  	postShopping(Employee: Employee){
  		return this.http.post(this.URL_API,Employee);
  	}
}
