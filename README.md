# Taximo challenge

En este archivo se describe la arquitectura y la implementacion de mi desarrollo

la aplicacion se desarrollo con node js y typescript. En la puesta en produccion se usa docker.


##Descripcion de los archivos

* controllers -->* shopping.ts --> **Logica del aplicativo.**
        
* entities --> * ShoppingSave.ts --> **Contiene la estructura de la tabla que guarda los resultados de los calculos.**

* routes --> * index.ts --> **Archivo que enruta los Request.**

    
## Clases
 
 * server/controllers/shopping.ts**
    
    - public Calculate
    - public setShop
    - public cleanData
    - public Validate
    - public saveResult
    

## Data model

### ShoppingSave
| id         | values     | time            |
| ---------- | ---------- | --------------- |
| number     | string     | string          |

## Project implementation

* clone el proyecto con el comando 

```
    git clone https://davidroa9708@bitbucket.org/davidroa9708/taximo.git
```

* instale las dependencias con el compando

```
    npm install
```
 
* ejecute el backend con el comando

```
    docker-compose up
```
 
* el backend sera accesible por la url

```
    localhost:3000/
```

* dentro de la carpeta frontend ejecute el el comando

```
    ng serve
```
 
* el frontend sera accesible por la url

```
    localhost:4200/
```

## URL EN AMAZON

```
    http://taximofront.s3-website-sa-east-1.amazonaws.com/
```
