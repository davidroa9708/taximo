FROM mhart/alpine-node:8.12.0

MAINTAINER JORGE DAVID ROA ESPITIA

RUN mkdir /app

WORKDIR /app

COPY . /app

RUN npm install

RUN npm install -g ts-node typescript 

EXPOSE 3000

CMD ["sh", "-c", "npm run watch-server"]
